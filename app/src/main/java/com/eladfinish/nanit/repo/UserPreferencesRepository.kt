package com.eladfinish.nanit.repo

import android.net.Uri
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

/**
 * Developed by
 * @author Elad Finish
 */

interface UserPreferencesRepository {
    suspend fun saveName(name: String)
    fun readName(): Flow<String>
    suspend fun saveDate(date: String)
    fun readDate(): Flow<String>
    suspend fun saveImageUri(uri: Uri)
    fun readImageUri(): Flow<String>
}

class UserPreferencesRepositoryImpl(
    private val dataStore: DataStore<Preferences>,
) : UserPreferencesRepository {

    private object Keys {
        val NAME: Preferences.Key<String> = stringPreferencesKey("name")
        val DATE: Preferences.Key<String> = stringPreferencesKey("date")
        val IMAGE_URI: Preferences.Key<String> = stringPreferencesKey("image_uri")
    }

    private inline val Preferences.name
        get() = this[Keys.NAME] ?: ""

    private inline val Preferences.date
        get() = this[Keys.DATE] ?: ""

    private inline val Preferences.imageUri
        get() = this[Keys.IMAGE_URI] ?: ""

    override suspend fun saveName(name: String) {
        dataStore.edit { it[Keys.NAME] = name }
    }

    override fun readName(): Flow<String> =
        dataStore.data.map { it.name }

    override suspend fun saveDate(date: String) {
        dataStore.edit { it[Keys.DATE] = date }
    }

    override fun readDate(): Flow<String> =
        dataStore.data.map { it.date }

    override suspend fun saveImageUri(uri: Uri) {
        dataStore.edit { it[Keys.IMAGE_URI] = uri.toString() }
    }

    override fun readImageUri(): Flow<String> =
        dataStore.data.map { it.imageUri }
}