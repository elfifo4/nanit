package com.eladfinish.nanit.model

import java.util.*

data class DateHolder(
    var year: Int,
    var month: Int,
    var dayOfMonth: Int,
) {
    companion object {
        fun getCurrentDate(): DateHolder {
            val calendar = Calendar.getInstance()
            val currentYear = calendar.get(Calendar.YEAR)
            val currentMonth = calendar.get(Calendar.MONTH)
            val currentDay = calendar.get(Calendar.DAY_OF_MONTH)
            return DateHolder(currentYear, currentMonth, currentDay)
        }
    }
}