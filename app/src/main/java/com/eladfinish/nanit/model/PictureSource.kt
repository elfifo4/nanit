package com.eladfinish.nanit.model

import android.content.Intent
import android.graphics.Bitmap
import android.provider.MediaStore
import androidx.annotation.StringRes
import com.eladfinish.nanit.R


enum class PictureSource(@StringRes val res: Int) {
    GALLERY(R.string.gallery),
    CAMERA(R.string.camera);

    companion object {
        const val PICK_IMAGE_REQUEST = 12345
        const val clearRes = R.string.clear

        fun toIntent(position: Int): Intent = when (position) {
            0 -> Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            1 -> Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            else -> throw IllegalArgumentException("Unexpected item position $position")
        }

        fun toBitmap(intent: Intent): Bitmap = intent.extras!!["data"] as Bitmap
    }
}