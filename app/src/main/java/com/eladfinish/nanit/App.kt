package com.eladfinish.nanit

import android.app.Application
import timber.log.Timber

/**
 * Developed by
 * @author Elad Finish
 */

@Suppress("unused")
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(
            object : Timber.DebugTree() {
                override fun createStackElementTag(element: StackTraceElement): String = element.formatted()
            }
        )
    }

    private fun StackTraceElement.formatted() = "($fileName:$lineNumber)[$methodName]"
}