package com.eladfinish.nanit.presentation.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.doOnLayout
import androidx.viewbinding.ViewBinding
import com.bumptech.glide.Glide
import com.eladfinish.nanit.databinding.FragmentBirthdayBinding
import com.eladfinish.nanit.extensions.getStatusBarHeight
import com.eladfinish.nanit.extensions.toast
import com.eladfinish.nanit.presentation.BirthdayUiResources
import com.eladfinish.nanit.presentation.UiEvent
import com.eladfinish.nanit.presentation.UiModel
import com.eladfinish.nanit.presentation.UserAction
import timber.log.Timber

/**
 * Developed by
 * @author Elad Finish
 */

class BirthdayFragment : BaseFragment() {

    override var _binding: ViewBinding? = null
    private val binding get() = _binding!! as FragmentBirthdayBinding

    private val uiResources by lazy(LazyThreadSafetyMode.NONE) { BirthdayUiResources(requireContext()) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        FragmentBirthdayBinding.inflate(inflater, container, false)
            .run {
                _binding = this
                root
            }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.birthdayRoot.setPadding(0, getStatusBarHeight(), 0, 0)

        binding.imagePlaceHolder.doOnLayout {
            (binding.buttonCamera.layoutParams as ConstraintLayout.LayoutParams)
                .apply {
                    circleRadius = it.width.div(2).minus(uiResources.strokeWidth).toInt()
                }
        }

        binding.buttonBack.setOnClickListener {
            viewModel.onEvent(UserAction.Birthday.BackClicked)
        }

        binding.buttonCamera.setOnClickListener {
            viewModel.onEvent(UserAction.Birthday.CameraClicked)
        }

        binding.buttonShare.setOnClickListener {
            viewModel.onEvent(
                UserAction.Birthday.ShareClicked(
                    exportedView = binding.root,
                    exportIgnoreViews = listOf(binding.buttonShare, binding.buttonBack, binding.buttonCamera)
                )
            )
        }
        var designIndex = 0
        viewModel.randomIndex().observe(viewLifecycleOwner) {
            designIndex = it
            val design: UiModel = uiResources.designs[it]
            with(binding) {
                backgroundImage.setBackgroundResource(design.background)
                birthdayRoot.setBackgroundResource(design.bgColor)
                buttonCamera.setBackgroundResource(design.camera)
                imagePlaceHolder.setStrokeColorResource(design.strokeColor)
            }
        }

        viewModel.readName().observe(viewLifecycleOwner) {
            binding.labelBabyName.text = uiResources.birthdayTitle(it)
        }

        viewModel.readAge().observe(viewLifecycleOwner) { (age, ageUnit) ->
            try {
                binding.babyAge.setImageResource(uiResources.getAgeDrawable(age))
                binding.labelMonthsYears.text = uiResources.getAgeUnitString(age, ageUnit)
            } catch (e: Exception) {
                Timber.e(e)
                requireContext().toast(e.message!!)
            }
        }

        viewModel.readImageUri().observe(viewLifecycleOwner) {
            Glide.with(this)
                .load(it)
                .placeholder(uiResources.designs[designIndex].placeholder)
                .circleCrop()
                .into(binding.imagePlaceHolder)
        }

        viewModel.birthdayUiEvents.observe(viewLifecycleOwner) {
            when (it) {
                UiEvent.Birthday.Back -> requireActivity().onBackPressed()
                UiEvent.Birthday.ShowImportImageChoices -> displayChoiceDialog(onClear = { viewModel.saveImageUri(Uri.EMPTY) })
                is UiEvent.Birthday.ShareIntent -> startActivity(Intent.createChooser(it.intent, uiResources.shareTitle))
            }
        }

    }
}