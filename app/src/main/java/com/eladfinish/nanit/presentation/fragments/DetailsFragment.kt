package com.eladfinish.nanit.presentation.fragments

import android.app.DatePickerDialog
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import com.bumptech.glide.Glide
import com.eladfinish.nanit.R
import com.eladfinish.nanit.databinding.FragmentDetailsBinding
import com.eladfinish.nanit.extensions.getStatusBarHeight
import com.eladfinish.nanit.extensions.hideKeyboard
import com.eladfinish.nanit.presentation.UiEvent
import com.eladfinish.nanit.presentation.UserAction

/**
 * Developed by
 * @author Elad Finish
 */

class DetailsFragment : BaseFragment() {

    override var _binding: ViewBinding? = null
    private val binding get() = _binding!! as FragmentDetailsBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        FragmentDetailsBinding.inflate(inflater, container, false)
            .run {
                _binding = this
                root
            }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.detailsRoot.setPadding(0, getStatusBarHeight(), 0, 0)

        binding.babyNameInput.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                viewModel.onEvent(UserAction.Details.OnInputLostFocus(getBabyName()))
            }
        }

        binding.babyNameInput.setOnEditorActionListener { _, actionId, _ ->
            when (actionId) {
                EditorInfo.IME_ACTION_DONE -> {
                    viewModel.onEvent(UserAction.Details.KeyboardDoneClicked(getBabyName()))
                    false //return false to close keyboard
                }
                else -> false
            }
        }

        binding.root.setOnClickListener {
            viewModel.onEvent(UserAction.Details.RootClicked(getBabyName()))
        }

        binding.birthdayButton.setOnClickListener {
            viewModel.onEvent(UserAction.Details.DateClicked)
        }

        binding.imageButton.setOnClickListener {
            viewModel.onEvent(UserAction.Details.ImageClicked)
        }

        binding.showBirthdayButton.setOnClickListener {
            viewModel.onEvent(UserAction.Details.BirthdayClicked)
        }

        viewModel.readName().observe(viewLifecycleOwner) {
            binding.babyNameInput.setText(it)
        }

        viewModel.readDateAsText().observe(viewLifecycleOwner) {
            binding.birthdayButton.text = it
        }

        viewModel.readImageUri().observe(viewLifecycleOwner) {
            Glide.with(this)
                .load(it)
                .circleCrop()
                .placeholder(R.drawable.ic_default_place_holder)
                .into(binding.imageButton)
        }

        viewModel.isBirthdayScreenEnabled().observe(viewLifecycleOwner) {
            binding.showBirthdayButton.isEnabled = it
        }

        viewModel.detailsUiEvents.observe(viewLifecycleOwner) {
            when (it) {
                UiEvent.Details.HideKeyboard -> hideKeyboard()
                UiEvent.Details.OpenBirthdayScreen -> findNavController().navigate(DetailsFragmentDirections.openBirthdayFragment())
                UiEvent.Details.ShowImportImageChoices -> displayChoiceDialog(onClear = { viewModel.saveImageUri(Uri.EMPTY) })
                is UiEvent.Details.ShowDatePicker -> {
                    binding.babyNameInput.clearFocus()
                    hideKeyboard()
                    DatePickerDialog(
                        requireContext(), { _, year, month, dayOfMonth ->
                            viewModel.saveDate(year, month, dayOfMonth)
                        },
                        it.year, it.month, it.dayOfMonth
                    ).show()
                }
            }
        }

    }

    private fun getBabyName() = binding.babyNameInput.text.toString()

}