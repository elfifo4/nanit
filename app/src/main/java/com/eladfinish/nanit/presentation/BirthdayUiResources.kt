package com.eladfinish.nanit.presentation

import android.content.Context
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import com.eladfinish.nanit.R
import java.time.temporal.ChronoUnit
import kotlin.LazyThreadSafetyMode.NONE

/**
 * Developed by
 * @author Elad Finish
 */

data class UiModel(
    @DrawableRes val background: Int,
    @DrawableRes val placeholder: Int,
    @DrawableRes val camera: Int,
    @ColorRes val bgColor: Int,
    @ColorRes val strokeColor: Int,
)

class BirthdayUiResources(private val context: Context) {

    val designs = listOf(
        UiModel(
            background = R.drawable.i_os_bg_elephant,
            placeholder = R.drawable.ic_default_place_holder_yellow,
            camera = R.drawable.ic_camera_icon_yellow,
            bgColor = R.color.bg_yellow,
            strokeColor = R.color.yellow,
        ),
        UiModel(
            background = R.drawable.i_os_bg_fox,
            placeholder = R.drawable.ic_default_place_holder_green,
            camera = R.drawable.ic_camera_icon_green,
            bgColor = R.color.bg_green,
            strokeColor = R.color.green,
        ),
        UiModel(
            background = R.drawable.i_os_bg_pelican_2,
            placeholder = R.drawable.ic_default_place_holder_blue,
            camera = R.drawable.ic_camera_icon_blue,
            bgColor = R.color.bg_blue,
            strokeColor = R.color.blue,
        ),
    )

    private val oldString by lazy(NONE) { context.getString(R.string.old) }
    val shareTitle by lazy(NONE) { context.getString(R.string.share_title) }
    val strokeWidth by lazy(NONE) { context.resources.getDimension(R.dimen.stroke_width) }

    fun birthdayTitle(babyName: String) = context.getString(R.string.birthday_title).format(babyName)

    fun getAgeUnitString(age: Int, ageUnit: ChronoUnit) =
        when (ageUnit) {
            ChronoUnit.MONTHS -> getMonths(age)
            ChronoUnit.YEARS -> getYears(age)
            else -> throw IllegalStateException("there is a problem with age ($age)")
        }.plus(" $oldString")

    private fun getMonths(count: Int) = context.resources.getQuantityString(R.plurals.age_months, count)
    private fun getYears(count: Int) = context.resources.getQuantityString(R.plurals.age_years, count)

    fun getAgeDrawable(age: Int) =
        when (age) {
            0 -> R.drawable.ic_age_number_0
            1 -> R.drawable.ic_age_number_1
            2 -> R.drawable.ic_age_number_2
            3 -> R.drawable.ic_age_number_3
            4 -> R.drawable.ic_age_number_4
            5 -> R.drawable.ic_age_number_5
            6 -> R.drawable.ic_age_number_6
            7 -> R.drawable.ic_age_number_7
            8 -> R.drawable.ic_age_number_8
            9 -> R.drawable.ic_age_number_9
            10 -> R.drawable.ic_age_number_10
            11 -> R.drawable.ic_age_number_11
            12 -> R.drawable.ic_age_number_12
            else -> throw IllegalArgumentException("Age must be in range 0..12 (was $age).")
        }
}