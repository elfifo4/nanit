package com.eladfinish.nanit.presentation

import android.content.Intent
import android.view.View

/**
 * Developed by
 * @author Elad Finish
 */

sealed class UiEvent {

    sealed class Details : UiEvent() {
        data class ShowDatePicker(val year: Int, val month: Int, val dayOfMonth: Int) : Details()
        object OpenBirthdayScreen : Details()
        object ShowImportImageChoices : Details()
        object HideKeyboard : Details()
    }

    sealed class Birthday : UiEvent() {
        object Back : Birthday()
        object ShowImportImageChoices : Birthday()
        data class ShareIntent(val intent: Intent) : Birthday()
    }

}

sealed class UserAction {

    sealed class Details {
        object BirthdayClicked : Details()
        object ImageClicked : Details()
        object DateClicked : Details()
        data class RootClicked(val babyName: String) : Details()
        data class OnInputLostFocus(val babyName: String) : Details()
        data class KeyboardDoneClicked(val babyName: String) : Details()
    }

    sealed class Birthday {
        object BackClicked : Birthday()
        object CameraClicked : Birthday()
        data class ShareClicked(val exportedView: View, val exportIgnoreViews: List<View>) : Birthday()
    }

}