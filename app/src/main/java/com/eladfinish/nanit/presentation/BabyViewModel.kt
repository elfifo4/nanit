package com.eladfinish.nanit.presentation

import android.app.Application
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import androidx.annotation.RequiresApi
import androidx.core.content.FileProvider
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import androidx.lifecycle.*
import com.eladfinish.nanit.R
import com.eladfinish.nanit.aux.SingleLiveEvent
import com.eladfinish.nanit.extensions.*
import com.eladfinish.nanit.model.DateHolder
import com.eladfinish.nanit.model.PictureSource
import com.eladfinish.nanit.repo.UserPreferencesRepository
import com.eladfinish.nanit.repo.UserPreferencesRepositoryImpl
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.time.Period
import java.time.temporal.ChronoUnit
import java.util.*

/**
 * Developed by
 * @author Elad Finish
 */

interface BabyViewModel {
    fun saveName(name: String)
    fun readName(): LiveData<String>

    fun saveDate(year: Int, month: Int, dayOfMonth: Int)
    fun readDateAsText(): LiveData<String>

    fun readAge(): LiveData<Pair<Int, ChronoUnit>>

    fun isBirthdayScreenEnabled(): LiveData<Boolean>

    fun handlePickImageIntent(intent: Intent)
    fun saveImageUri(uri: Uri)
    fun readImageUri(): LiveData<Uri>
    fun randomIndex(): LiveData<Int>

    val detailsUiEvents: LiveData<UiEvent.Details>
    fun onEvent(action: UserAction.Details)

    val birthdayUiEvents: LiveData<UiEvent.Birthday>
    fun onEvent(action: UserAction.Birthday)
}

class BabyViewModelImpl(app: Application) : AndroidViewModel(app), BabyViewModel {

    companion object {
        private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "user_preferences")
        private const val SEPARATOR = "/"
        private const val fileName = "baby_picture"
        private const val fileType = "jpg"
    }

    init {
        setupDate()
    }

    private val compositeDisposable = CompositeDisposable()

    private val path: String by lazy { Environment.DIRECTORY_PICTURES + File.separator + app.getString(R.string.app_name) }

    private val context: Context get() = getApplication<Application>()

    private val userPreferencesRepository: UserPreferencesRepository by lazy {
        UserPreferencesRepositoryImpl(context.dataStore)
    }

    private val date = DateHolder.getCurrentDate()

    override fun saveName(name: String) {
        viewModelScope.launch(Dispatchers.IO) { userPreferencesRepository.saveName(name) }
    }

    override fun readName(): LiveData<String> =
        userPreferencesRepository.readName().asLiveData()

    override fun saveDate(year: Int, month: Int, dayOfMonth: Int) {
        /*
        DatePickerDialog OnDateSetListener params:
        year – the selected year
        month – the selected month (0-11 for compatibility with Calendar.MONTH)
        dayOfMonth – the selected day of the month (1-31, depending on month)
        */
        val date = "${dayOfMonth.padDate()}$SEPARATOR${month.plus(1).padDate()}$SEPARATOR$year"
        viewModelScope.launch(Dispatchers.IO) { userPreferencesRepository.saveDate(date) }
    }

    override fun readDateAsText(): LiveData<String> =
        readDate().map {
            it.first
        }

    private fun readDate(): LiveData<Pair<String, List<Int>>> =
        readDateInternal().filter { (_, list) ->
            list.size == 3
        }

    private fun readDateInternal(): LiveData<Pair<String, List<Int>>> =
        userPreferencesRepository.readDate().asLiveData().map { date ->
            date to date.split(SEPARATOR)
        }.map {
            if (it.second.size == 1) {
                it.first to emptyList()
            } else {
                it.first to it.second.map { s -> if (s.isEmpty()) -1 else s.toInt() }
            }
        }

    private fun setupDate() {
        viewModelScope.launch(Dispatchers.IO) {
            readDate().asFlow().collect { (_, list) ->
                with(date) {
                    year = list[2]
                    month = list[1].minus(1)
                    dayOfMonth = list[0]
                }
            }
        }
    }

    override fun isBirthdayScreenEnabled(): LiveData<Boolean> =
        userPreferencesRepository.readName().combine(userPreferencesRepository.readDate()) { name, date ->
            name.isNotEmpty() && date.isNotEmpty()
        }.asLiveData()

    override fun readAge(): LiveData<Pair<Int, ChronoUnit>> =
        readDate().switchMap { (_, list) ->
            val currentDate: Date = Calendar.getInstance().time
            val year = list[2]
            val month = list[1].minus(1)
            val day = list[0]

            val calendar = Calendar.getInstance().apply {
                set(year, month, day)
            }
            val birthday: Date = calendar.time
            val period = getPeriodBetweenDates(startDate = birthday, endDate = currentDate)
            Timber.d("viewModel getDiffInMonths period $period")

            if (period.years == 0) {//during the first year of the baby
                MutableLiveData(period.months to ChronoUnit.MONTHS)
            } else {//when the baby is a one year old or older
                MutableLiveData(period.years to ChronoUnit.YEARS)
            }
        }

    override fun handlePickImageIntent(intent: Intent) {
        val selectedImageUri: Uri? = intent.data
        when {
            selectedImageUri != null -> { //from gallery
                saveImageUri(selectedImageUri)
            }
            else -> { //from camera
                val bitmap = PictureSource.toBitmap(intent)
                val imageUri = saveImage(bitmap)
                saveImageUri(imageUri)
            }
        }
    }

    override fun saveImageUri(uri: Uri) {
        viewModelScope.launch(Dispatchers.IO) { userPreferencesRepository.saveImageUri(uri) }
    }

    private fun shareBirthdayScreen(exportedView: View, exportIgnoreViews: List<View>) {
        Observable.just(exportedView)
            .subscribeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { context.toast("Sharing screen...") }
            .doOnNext { exportIgnoreViews.forEach { it.visibility = View.INVISIBLE } }
            .map { createBitmapFromView(it, it.width, it.height) }
            .doOnNext { exportIgnoreViews.forEach { it.visibility = View.VISIBLE } }
            .observeOn(Schedulers.io())
            .zipWith(Observable.just(File("${context.cacheDir}/nanit_image.png"))) { bitmap, file ->
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, FileOutputStream(file))
                FileProvider.getUriForFile(context, context.getString(R.string.file_provider_authority), file)
            }
            .map {
//                val playStore = "https://play.google.com/store/apps/details?id=com.nanit.baby"
                val website = "https://www.nanit.com"
                val shareText = "Shared from *Nanit*\n$website"
                Intent(Intent.ACTION_SEND).apply {
                    putExtra(Intent.EXTRA_STREAM, it)
                    putExtra(Intent.EXTRA_TEXT, shareText)
                    type = "image/jpeg"
                }
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    _birthdayUiEvents.postValue(UiEvent.Birthday.ShareIntent(it))
                },
                {
                    Timber.e(it)
                    context.toast(it.message ?: "Error")
                }
            ).addTo(compositeDisposable)
    }

    override fun readImageUri(): LiveData<Uri> =
        userPreferencesRepository.readImageUri().asLiveData().map { Uri.parse(it) }

    override fun randomIndex(): LiveData<Int> = MutableLiveData((0..2).random())

    override val detailsUiEvents: LiveData<UiEvent.Details> get() = _detailsUiEvents
    private val _detailsUiEvents: MutableLiveData<UiEvent.Details> = SingleLiveEvent()

    override val birthdayUiEvents: LiveData<UiEvent.Birthday> get() = _birthdayUiEvents
    private val _birthdayUiEvents: MutableLiveData<UiEvent.Birthday> = SingleLiveEvent()

    private fun saveImage(bitmap: Bitmap): Uri {
        val imageUri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            createUriAfterQ()
        } else {
            createUriForPreQ()
        }
        val resolver = context.contentResolver
        val fos: OutputStream = resolver.openOutputStream(imageUri!!)!!
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
        return imageUri
    }

    @Throws(IOException::class)
    private fun createUriForPreQ(): Uri? {
        val storageDir = Environment.getExternalStoragePublicDirectory(path)
        val mFile = File.createTempFile(fileName, fileType, storageDir)
        return FileProvider.getUriForFile(context, context.getString(R.string.file_provider_authorities), mFile)
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun createUriAfterQ(): Uri? {
        val resolver = context.contentResolver
        val contentValues = ContentValues().apply {
            put(MediaStore.MediaColumns.DISPLAY_NAME, "$fileName.$fileType")
            put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
            put(MediaStore.MediaColumns.RELATIVE_PATH, path)
        }
        // TODO always overwrite the same generated file to avoid duplicates such as:
        //baby_picture.jpg, baby_picture (1).jpg,baby_picture (2).jpg,baby_picture (3).jpg,

        return resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
    }

    private fun getPeriodBetweenDates(startDate: Date, endDate: Date): Period =
        Period.between(startDate.toLocalDate(), endDate.toLocalDate())

    private fun createBitmapFromView(view: View, width: Int, height: Int): Bitmap {
        Timber.d("export_bitmap width $width height $height")
        if (width > 0 && height > 0) {
            view.measure(
                View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY)
            )
        }
        view.layout(0, 0, view.measuredWidth, view.measuredHeight)

        val bitmap = Bitmap.createBitmap(
            view.measuredWidth,
            view.measuredHeight, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        val background = view.background

        background?.draw(canvas)
        view.draw(canvas)

        return bitmap
    }

    override fun onEvent(action: UserAction.Birthday) {
        when (action) {
            UserAction.Birthday.BackClicked -> _birthdayUiEvents.postValue(UiEvent.Birthday.Back)
            UserAction.Birthday.CameraClicked -> _birthdayUiEvents.postValue(UiEvent.Birthday.ShowImportImageChoices)
            is UserAction.Birthday.ShareClicked -> shareBirthdayScreen(action.exportedView, action.exportIgnoreViews)
        }
    }

    override fun onEvent(action: UserAction.Details) {
        when (action) {
            UserAction.Details.BirthdayClicked -> _detailsUiEvents.postValue(UiEvent.Details.OpenBirthdayScreen)
            UserAction.Details.ImageClicked -> _detailsUiEvents.postValue(UiEvent.Details.ShowImportImageChoices)
            UserAction.Details.DateClicked -> {
                _detailsUiEvents.postValue(
                    UiEvent.Details.ShowDatePicker(
                        year = date.year,
                        month = date.month,
                        dayOfMonth = date.dayOfMonth
                    )
                )
            }
            is UserAction.Details.KeyboardDoneClicked -> saveName(action.babyName)
            is UserAction.Details.OnInputLostFocus -> saveName(action.babyName)
            is UserAction.Details.RootClicked -> {
                saveName(action.babyName)
                _detailsUiEvents.postValue(UiEvent.Details.HideKeyboard)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}