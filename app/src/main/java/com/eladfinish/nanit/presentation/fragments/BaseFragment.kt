package com.eladfinish.nanit.presentation.fragments

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.viewbinding.ViewBinding
import com.eladfinish.nanit.BuildConfig
import com.eladfinish.nanit.R
import com.eladfinish.nanit.aux.viewModelFactory
import com.eladfinish.nanit.extensions.runIf
import com.eladfinish.nanit.model.PictureSource
import com.eladfinish.nanit.presentation.BabyViewModel
import com.eladfinish.nanit.presentation.BabyViewModelImpl
import timber.log.Timber

/**
 * Developed by
 * @author Elad Finish
 */

abstract class BaseFragment : Fragment() {

    @Suppress("PropertyName")
    protected abstract var _binding: ViewBinding?

    protected val viewModel: BabyViewModel by viewModels {
        viewModelFactory { BabyViewModelImpl(requireActivity().application) }
    }

    protected fun displayChoiceDialog(onClear: () -> Unit) {
        AlertDialog.Builder(requireContext()).apply {
            setIcon(android.R.drawable.ic_menu_camera)
            setTitle(getString(R.string.dialog_image_picker_title))
            setItems(
                PictureSource.values().map { getString(it.res) }.toTypedArray()
                    .runIf(BuildConfig.DEBUG) { plus(getString(PictureSource.clearRes)) }) { _, which ->
                if (BuildConfig.DEBUG && which == 2) {
                    onClear.invoke()
                    return@setItems
                }
                val intent: Intent = PictureSource.toIntent(which)
                startActivityForResult(
                    Intent.createChooser(intent, getString(R.string.image_chooser_title)),
                    PictureSource.PICK_IMAGE_REQUEST
                )
            }.show()
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PictureSource.PICK_IMAGE_REQUEST) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                viewModel.handlePickImageIntent(data)
            } else {
                Timber.d("Image picker canceled!")
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}