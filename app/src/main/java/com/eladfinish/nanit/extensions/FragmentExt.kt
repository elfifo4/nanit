package com.eladfinish.nanit.extensions

import androidx.fragment.app.Fragment

fun Fragment.getStatusBarHeight(): Int =
    resources.getIdentifier("status_bar_height", "dimen", "android").let {
        resources.getDimensionPixelSize(it).takeIf { size -> size > 0 } ?: 0
    }