package com.eladfinish.nanit.extensions

import android.content.Context
import android.widget.Toast

fun Context.toast(messageId: Int, duration: Int = Toast.LENGTH_SHORT) = toast(getString(messageId), duration)

fun Context.toast(message: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, duration).show()
}

fun Context.getScreenWidth(): Int = resources.displayMetrics.widthPixels