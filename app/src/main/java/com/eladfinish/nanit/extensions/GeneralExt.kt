package com.eladfinish.nanit.extensions

import com.eladfinish.nanit.BuildConfig

inline fun <T> T.runIf(condition: Boolean, block: T.() -> T): T = if (condition) block() else this

inline fun runIfDebug(block: () -> Unit): Unit = if (BuildConfig.DEBUG) block() else Unit

inline fun <T> T.runIfNot(condition: Boolean, block: T.() -> T): T = if (condition.not()) block() else this
