package com.eladfinish.nanit.extensions

fun Int.padDate(): String = toString().padStart(2, '0')
