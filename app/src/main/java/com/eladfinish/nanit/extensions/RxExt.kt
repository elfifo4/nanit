package com.eladfinish.nanit.extensions

import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.disposables.DisposableContainer

fun <T : Disposable> T.addTo(compositeDisposable: DisposableContainer): T {
    compositeDisposable.add(this)
    return this
}